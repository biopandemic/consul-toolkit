build:
	docker build -t consul-toolkit:latest .

run:
	docker run -it consul-toolkit:latest sh
run/consul:
	docker run -it consul-toolkit:latest consul info

run/consul-template:
	docker run -it consul-toolkit:latest consul-template

run/envconsul:
	docker run -it consul-toolkit:latest envconsul
