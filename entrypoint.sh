#!/bin/bash
readonly BIN=$1; shift

case $BIN in
	consul) consul $@ ;;
	consul-template) consul-template $@ ;;
	envconsul) envconsul $@ ;;
	sh) /bin/bash ;;
	*)
		echo "BIN not listed for execution, failing!" >&2
		exit -1
		;;
esac
