# Name :: consul-toolkit
#
# USAGE ::
#   docker build -t jacoblogemann/consul-toolkit .
#   docker run -v /var/run/docker.sock:/tmp/docker.sock \
#     -it jacoblogemann/consul-template [...args]
#
# URLS ::
#   https://github.com/hashicorp/consul-template
#   https://github.com/hashicorp/consul
#
FROM alpine:3.2
MAINTAINER Dyn Automation <automation@dyn.com>

ENV SERVICE_NAME consul-template
ENV SERVICE_TAGS consul,backend


# ------------------------------------------------------------------------
# Install common deps.
# ------------------------------------------------------------------------
RUN apk --update add bash curl ca-certificates && \
    curl -Ls https://circle-artifacts.com/gh/andyshinn/alpine-pkg-glibc/6/artifacts/0/home/ubuntu/alpine-pkg-glibc/packages/x86_64/glibc-2.21-r2.apk > /tmp/glibc-2.21-r2.apk && \
    apk add --allow-untrusted /tmp/glibc-2.21-r2.apk && \
    rm -rf /tmp/glibc-2.21-r2.apk /var/cache/apk/*


# ------------------------------------------------------------------------
# Install consul agent
# ------------------------------------------------------------------------
ENV CONSUL_VERSION 0.5.2
ENV CONSUL_ARCH linux_amd64
ENV CONSUL_ARCHIVE ${CONSUL_VERSION}_${CONSUL_ARCH}
ENV CONSUL_URL https://dl.bintray.com/mitchellh/consul/
ENV CONSUL_SRC ${CONSUL_URL}/${CONSUL_ARCHIVE}.zip

ADD https://dl.bintray.com/mitchellh/consul/0.5.2_linux_amd64.zip /tmp/consul.zip
RUN cd /bin && unzip /tmp/consul.zip && chmod +x /bin/consul && rm /tmp/consul.zip
## Install Hashicorp's "consul" binary.
ADD ${CONSUL_SRC} /tmp/consul.zip
RUN cd /tmp && \
    unzip /tmp/consul.zip && \
    chmod +x /tmp/consul && \
    mv /tmp/consul /bin/consul && \
    rm -f /tmp/consul.zip


# ------------------------------------------------------------------------
# Install consul-template
# ------------------------------------------------------------------------
ENV CONSULT_VERSION 0.10.0
ENV CONSULT_ARCH linux_amd64
ENV CONSULT_ARCHIVE consul-template_${CONSULT_VERSION}_${CONSULT_ARCH}
ENV CONSULT_URL https://github.com/hashicorp/consul-template/releases/download/
ENV CONSULT_SRC ${CONSULT_URL}/v${CONSULT_VERSION}/${CONSULT_ARCHIVE}.tar.gz

## Install Hashicorp's "consul-template" binary.
ADD ${CONSULT_SRC} /tmp/consul-template.tar.gz
RUN cd /tmp && \
    gzip -dc /tmp/consul-template.tar.gz | tar -xf - && \
    rm /tmp/consul-template.tar.gz && \
    mv /tmp/${CONSULT_ARCHIVE}/consul-template \
       /bin/consul-template && \
    chmod +x /bin/consul-template && \
    rm -rf /tmp/${CONSULT_ARCHIVE}




# ------------------------------------------------------------------------
# Install envconsul
# ------------------------------------------------------------------------
ENV ENVCONSUL_VERSION 0.5.0
ENV ENVCONSUL_ARCH linux_amd64
ENV ENVCONSUL_ARCHIVE envconsul_${ENVCONSUL_VERSION}_${ENVCONSUL_ARCH}
ENV ENVCONSUL_URL https://github.com/hashicorp/envconsul/releases/download/
ENV ENVCONSUL_SRC ${ENVCONSUL_URL}/v${ENVCONSUL_VERSION}/${ENVCONSUL_ARCHIVE}.tar.gz

## Install Hashicorp's "consul-template" binary.
ADD ${ENVCONSUL_SRC} /tmp/envconsul.tar.gz
RUN cd /tmp && \
    gzip -dc /tmp/envconsul.tar.gz | tar -xf - && \
    rm /tmp/envconsul.tar.gz && \
    mv /tmp/${ENVCONSUL_ARCHIVE}/envconsul \
       /bin/envconsul && \
    chmod +x /bin/envconsul


# ------------------------------------------------------------------------
# Install docker client
# ------------------------------------------------------------------------
ENV DOCKER_HOST unix:///tmp/docker.sock
ENV DOCKER_SRC https://get.docker.com/builds/Linux/x86_64/docker-latest
ADD ${DOCKER_SRC} /bin/docker
RUN chmod +x /bin/docker

ENV DOCKER_HOST unix:///tmp/docker.sock
VOLUME /tmp/docker.sock


# ------------------------------------------------------------------------
# Create directory for consul configs.
# ------------------------------------------------------------------------
RUN mkdir -p /consul/config /consul/data
WORKDIR /consul
VOLUME /consul


# ------------------------------------------------------------------------
# Add the current directory to root of the container
#   file system (see .dockerignore for any exceptions).
# ------------------------------------------------------------------------
ADD . /


# ------------------------------------------------------------------------
# Add custom entry point to docker container to limit possible commands.
# ------------------------------------------------------------------------
ADD ./entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]
CMD []
